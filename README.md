# Amazon Kinesis Anallytics v2 Connector
Amazon Kinesis Data Analytics is a fully managed service that you can use to process and analyze streaming data using Java, SQL, or Scala. The service enables you to quickly author and run Java, SQL, or Scala code against streaming sources to perform time series analytics, feed real-time dashboards, and create real-time metrics.

Documentation: https://docs.aws.amazon.com/kinesisanalytics/

Specification: https://raw.githubusercontent.com/APIs-guru/openapi-directory/main/APIs/amazonaws.com/kinesisanalyticsv2/2018-05-23/openapi.yaml

## Prerequisites

+ Your kinesis server, matching the pattern https://kinesisanalytics.{region}.{domain}. Example: https://kinesisanalytics.us-east-1.amazonaws.com
+ Generate an AWS Acess Key ID and AWS Secret Key from the AWS Admin Console

## Supported Operations
**All end points are passing.**
## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

